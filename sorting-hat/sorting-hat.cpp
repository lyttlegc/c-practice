#include <iostream>
using namespace std;

int main() {
    // Creates variables for each of the houses
    int gryffindor, hufflepuff, ravenclaw, slytherin;
    int answer1, answer2, answer3, answer4;
    int max = 0;
    string house;

    // Welcomes the user to the quiz
    cout << "Welcome to the Sorting Hat Quiz!\n\n";

    // First question, user inputs an answer that is assigned to answer1
    cout << "Q1) When I'm dead, I want people to remember me as: \n\n";
    cout << "1) The Good\n2) The Great\n3) The Wise\n4) The Bold\n\n";
    cin >> answer1;

    // Assigns a point to specific house depending on answer
    if (answer1 == 1) {
        hufflepuff ++;
    }
    else if (answer1 == 2) {
        slytherin ++;
    }
    else if (answer1 == 3) {
        ravenclaw ++;
    }
    else if (answer1 == 4) {
        gryffindor ++;
    }
    else {
        cout << "Invalid input\n";
    }

    // Second question, user inputs an answer that is assigned to answer2
    cout << "Q2) Dawn or Dusk? \n\n";
    cout << "1) Dawn\n2) Dusk\n\n";
    cin >> answer2;

    // Assigns a point to two houses depending on answer
    if (answer2 == 1) {
        gryffindor ++;
        ravenclaw ++;
    }
    else if (answer2 == 2) {
        slytherin ++;
        hufflepuff ++;
    }
    else {
        cout << "Invalid input\n";
    }

    // Third question, user inputs an answer that is assigned to answer3
    cout << "Q3) Which kind of instrument most pleases your ear? \n\n";
    cout << "1) The violin\n2) The trumpet\n3) The piano\n4) The drum\n\n";
    cin >> answer3;

    // Assigns a point to specific house depending on answer
    if (answer3 == 1) {
        slytherin ++;
    }
    else if (answer3 == 2) {
        hufflepuff ++;
    }
    else if (answer3 == 3) {
        ravenclaw ++;
    }
    else if (answer3 == 4) {
        gryffindor ++;
    }
    else {
        cout << "Invalid input\n";
    }

    // Fourth question, user inputs an answer that is assigned to answer4
    cout << "Q3) Which road tempts you most? \n\n";
    cout << "1) The wide, sunny grassy lane\n2) The narrow, darl, lantern-lit alley\n";
    cout << "3) The The twisting, leaf-strewn path through the woods\n";
    cout << "4) The cobbled street lined (ancient buildings)\n\n";
    cin >> answer4;

    // Assigns a point to specific house depending on answer
    if (answer4 == 1) {
        hufflepuff ++;
    }
    else if (answer4 == 2) {
        slytherin ++;
    }
    else if (answer4 == 3) {
        gryffindor ++;
    }
    else if (answer4 == 4) {
        ravenclaw ++;
    }
    else {
        cout << "Invalid input\n";
    }

    // Finds which house the user belongs to and awards them their answer
    if (gryffindor > max) {
        max = gryffindor;
        house = "Gryffindor";
    }
    if (hufflepuff > max) {
        max = hufflepuff;
        house = "Hufflepuff";
    }
    if (ravenclaw > max) {
        max = ravenclaw;
        house = "Ravenclaw";
    }
    if (slytherin > max) {
        max = slytherin;
        house = "Slytherin";
    }

    cout << house << "!\n";
}
