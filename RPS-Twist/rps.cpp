#include <iostream>
#include <stdlib.h>
using namespace std;

/*
This is a game of rock paper scissors vs "the computer"
*/
int main() {
    srand (time(NULL));

    int computer = rand() % 3 + 1;
    int user = 0;

    cout << "==============\n";
    cout << "Rock, Paper, or Scissors?\n";
    cout << "==============\n";

    cout << "(Choose a number, 1-3)\n";
    cout << "1) Rock\n";
    cout << "2) Paper\n";
    cout << "3) Scissors\n";
    cout << "Shoot!\n";
    cin >> user;

    if (user == 1 && computer == 1) {
        cout << "The computer chose ROCK. It's a TIE!";
    }
    else if (user == 1 && computer == 2) {
        cout << "The computer chose PAPER. You LOSE!";
    }
    else if (user == 1 && computer == 3) {
        cout << "The computer chose SCISSORS. You WIN!";
    }
    else if (user == 2 && computer == 1) {
        cout << "The computer chose ROCK. You WIN!";
    }
    else if (user == 2 && computer == 2) {
        cout << "The computer chose PAPER. It's a TIE!";
    }
    else if (user == 2 && computer == 3) {
        cout << "The computer chose SCISSORS. You LOSE!";
    }
    else if (user == 3 && computer == 1) {
        cout << "The computer chose ROCK. You Lose!";
    }
    else if (user == 3 && computer == 2) {
        cout << "The computer chose PAPER. It's a WIN!";
    }
    else if (user == 3 && computer == 3) {
        cout << "The computer chose SCISSORS. It's a TIE!";
    }
    else {
        cout << "Invalid input. TRY AGAIN.";
    }

}
